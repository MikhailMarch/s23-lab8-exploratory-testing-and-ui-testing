##First case
This test navigates to the USA.gov homepage, clicks on the "Housing" link, goes to Find subsidized rental housing and checks whether the header is present 

##Second case
In this example, the test case navigates to the taxes page, then to the get help filling the taxes, then checks weather it contains the Get free help with your tax return 

##Third case
This test case navigates to the USA.gov website and searches for information about passports. It then checks that the search results contain relevant information, specifically instructions on how to apply for a passport and how to renew a passport.