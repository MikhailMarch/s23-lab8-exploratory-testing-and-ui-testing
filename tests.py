import pytest
import time


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

us_url = "https://www.usa.gov"

driver = webdriver.Chrome(ChromeDriverManager().install())


# Taxes
driver.get(us_url)

taxes = driver.find_elements(By.XPATH, '//a[contains(@href,"/taxes")]')

taxes[1].click()

tax_refunds = driver.find_element(By.XPATH, "//a[contains(text(), 'Get help filing your taxes')]")

tax_refunds.click()

help_filing = driver.find_element(By.XPATH, '//a[contains(@href,"/help-filing-taxes")]')

print(help_filing.find_element(By.XPATH, "//header").find_element(By.XPATH, "//h2").text)
assert "Get free help with your tax return" == help_filing.find_element(By.XPATH, "//header").find_element(By.XPATH, "//h2").text

time.sleep(1)


# Check search
driver.get(us_url)

search_box = driver.find_element(By.ID, "search-field-small")
search_box.send_keys("passport")
search_box.send_keys(Keys.RETURN)

search_results = driver.find_elements(By.XPATH, '//h4[@class="title"]')

assert any("U.S. passports | USAGov" in result.text for result in search_results)
assert any("Need a Passport - United States Department of State" in result.text for result in search_results)

time.sleep(1)


#Check housing
driver.get(us_url)

housing = driver.find_element(By.XPATH, "//a[contains(text(),'Locate affordable rental housing')]")
housing.click()

# Click on "Locate affordable rental housing" link
rental_housing = driver.find_element(By.XPATH, "//h2[contains(text(), 'Find subsidized rental housing')]")
rental_housing.click()

headers = driver.find_elements(By.XPATH, "//p[@class='usa-intro']")

assert ("In subsidized housing, the government pays apartment owners to offer reduced rents to tenants with low incomes. Learn how to find this type of affordable housing." in header.text for header in headers)

driver.quit()